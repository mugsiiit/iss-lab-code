## Structure
### Dog.py contains basic class structure
### inheritance.py explains how child classes are made and used

## The dirctory "banking" contains the code for example questions in the lab task
#### first open bankAccount.py, it contains the parent class
#### savingsAccount.py contains the child class of BankAccount
#### feeSavingsAccount.py contains the child class of SavingsAccount
