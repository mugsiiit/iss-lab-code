'''
Using the previous class as a "parent" class,
we look at inheritance of one class into the other.
'''

# Same class as the previous file
# Could have imported the file here, but copy-pasted for clarity

class Dog:

    # Class Attribute
    # Same for all instances
    species = 'mammal'

    # Initializer / Instance Attributes
    # Different for different instances
    # Needs at least one argument apart from self
    def __init__(self, name, age):
        self.name = name
        self.age = age

    # instance method
    def description(self):
        return "{} is {} years old".format(self.name, self.age)

    # instance method
    def speak(self, sound):
        return "{} says {}".format(self.name, sound)

    # instance method
    def birthday(self):
        oldAge = self.age
        self.age=self.age+1
        return "{}'s old age was {}, and new age is {}".format(self.name, oldAge, self.age)


'''
Consider 2 breeds:
Pug
Beagle
'''
class Pug(Dog):
	def run(self, speed):
		return "{}'s speed is {}".format(self.name, speed)


class Beagle(Dog):
	def numColours(self, colour):
		return "{} has {} colours".format(self.name, colour)


# Child classes inherit attributes and
# behaviors from the parent class
tommy = Pug("Tommy", 12)
print(tommy.description())

# Child classes have specific attributes
# and behaviors as well
print(tommy.run("slow"))
print(tommy.run("30 kmph"))


'''
Overriding functionality of parent class
Puppy's species will override that of its parent class
'''
class Puppy(Dog):
	species = 'small mammal'

bruno = Beagle("Bruno", 8)
print("Bruno's species: ", bruno.species)

buddy = Puppy("Buddy", 2)
print("Buddy's species: ", buddy.species)

