from savingsAccount import SavingsAccount

class FeeSavingsAccount(SavingsAccount):	
    def withdraw(self, pin, amount):
        """Decrement account balance by amount and return amount withdrawn."""
        amount-=1
        if pin==self.get_pin():
            if self.balance >= amount:
                self.balance -= amount
                return amount
            else:
                return "Balance not sufficient"

feeSav=FeeSavingsAccount(2222)
feeSav.deposit(2222,400)
print(feeSav.withdraw(2222,23))